package chapter6.service;

import static chapter6.utils.CloseableUtil.*;
import static chapter6.utils.DBUtil.*;

import java.sql.Connection;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.UserMessage;
import chapter6.dao.MessageDao;
import chapter6.dao.UserMessageDao;

public class MessageService {

    public void insert(Message message) {

        Connection connection = null;
        try {
            connection = getConnection();
            new MessageDao().insert(connection, message);
            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

    public List<UserMessage> select(String userId, String startDay, String endDay) {
        final int LIMIT_NUM = 1000;

        Connection connection = null;
        try {
            connection = getConnection();

            Integer id = null;
            if(!StringUtils.isEmpty(userId)) {
            	id = Integer.parseInt(userId);
            }

            String start = null;
            if (!StringUtils.isEmpty(startDay)) {
            	start = startDay + " " + "00:00:00";
            } else {
            	start = "2022-03-01 00:00:00";
            }

            String end = null;
            if (!StringUtils.isEmpty(endDay)) {
            	end = endDay + " " + "23:59:59";
            } else {
            	Date day = new Date();
            	SimpleDateFormat endFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                end = endFormat.format(day);
            }

            List<UserMessage> messages = new UserMessageDao().select(connection, id, LIMIT_NUM, start, end);
            commit(connection);

            return messages;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }

	public void delete(int deleteMessage) {

		 Connection connection = null;
	        try {
	            connection = getConnection();
	            new MessageDao().delete(connection, deleteMessage);
	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }

	}

	public void update(Message message) {

		 Connection connection = null;
	        try {
	            connection = getConnection();
	            new MessageDao().update(connection, message);
	            commit(connection);
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }

	}

	public Message select(int id) {

		 Connection connection = null;
	        try {
	            connection = getConnection();
	            Message message = new MessageDao().select(connection, id);

	            commit(connection);
	            return message;
	        } catch (RuntimeException e) {
	            rollback(connection);
	            throw e;
	        } catch (Error e) {
	            rollback(connection);
	            throw e;
	        } finally {
	            close(connection);
	        }
	}

}



