package chapter6.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import chapter6.beans.Message;
import chapter6.beans.User;
import chapter6.service.MessageService;



@WebServlet(urlPatterns = { "/edit" })
public class EditServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    	HttpSession session = request.getSession();

    	String messageId = request.getParameter("id");

    	Message message = null;

    	if (!StringUtils.isBlank(messageId) && messageId.matches("^[0-9]+$")) {

    		message = new MessageService().select(Integer.parseInt(request.getParameter("id")));
    	}

    	if (message == null) {
            session.setAttribute("errorMessages", "不正なパラメータが入力されました");
            response.sendRedirect("./");
            return;

    	}

    	request.setAttribute("user", (User) session.getAttribute("loginUser"));
    	request.setAttribute("message", message);
    	request.getRequestDispatcher("edit.jsp").forward(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

    	 HttpSession session = request.getSession();
         List<String> errorMessages = new ArrayList<String>();
         User loginUser = (User) session.getAttribute("loginUser");

         String text = request.getParameter("text");

         Message editMessage = new Message();
		 editMessage.setId(Integer.parseInt(request.getParameter("id")));

         if (!isValid(text, errorMessages)) {
             request.setAttribute("errorMessages", errorMessages);
             request.setAttribute("message", editMessage);
             request.setAttribute("user", loginUser);
             request.getRequestDispatcher("edit.jsp").forward(request, response);
             return;
         }

		 editMessage.setText(text);

    	new MessageService().update(editMessage);
    	response.sendRedirect("./");
    }

    private boolean isValid(String text, List<String> errorMessages) {

        if (StringUtils.isBlank(text)) {
            errorMessages.add("メッセージを入力してください");
        } else if (140 < text.length()) {
            errorMessages.add("140文字以下で入力してください");
        }

        if (errorMessages.size() != 0) {
            return false;
        }
        return true;
    }

}
